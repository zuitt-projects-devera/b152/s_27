let http = require("http");

//Mock Data

const users = [
{
	username: "moonknight1999",
	email: "moonGod@gmail.com",
	password: "moonKnightStrong"
},
{
	username: "kitkatMachine",
	email: "notSponsored@gmail.com",
	password: "kitkatForever"
}
]

const courses = [
{
	name: "Science 101",
	price: 2500,
	isActive: false
},
{
	name: "Filipino 101",
	price: 3000,
	isActive: true
}
]
/*courses.push({
	name: "English 101",
	price: 900,
	isActive: true
}*/
http.createServer(function(req,res){

//console.log(req.url); //endpoint
//console.log(req.method);


/*
We can think of request methods as the request/client telling our server 
the action to take for their request.

This way we don't need to have different endpoints for everything that
our client wants to do.

HTTP methods allow us to group and organize our routes.

With this we can have the same endpoints but with different methods. 

HTTPS methods are particularly and primarily concerned with CRUD operations.

Common HTTP Methods: 

GET - Get method for request indicates that the client/request 
want to retrieve or get data.

POST - Post method for requests indicates that the client/request wants to post data and create a document.

PUT - Put method for request indicates that the client/request want to input data
and update a document.

DELETE - Delete method for request indicates that the client/request wants 
to delete a document.


*/
if (req.url === "/" && req.method === "GET") {
	res.writeHead(200, {"Content-Type": "text/plain"});
	res.end("HELLO, This route checks for GET Method.");
}
else if (req.url === "/" && req.method === "POST") {
	res.writeHead(200, {"Content-Type": "text/plain"});
	res.end("HELLO, This route checks for POST Method.");
}
else if (req.url === "/" && req.method === "PUT") {
	res.writeHead(200, {"Content-Type": "text/plain"});
	res.end("HELLO, This route checks for PUT Method.");
}
else if (req.url === "/" && req.method === "DELETE") {
	res.writeHead(200, {"Content-Type": "text/plain"});
	res.end("HELLO, This route checks for DELETE Method.");
}
else if (req.url === "/users" && req.method === "POST") {
	//This route should allow us to receive data input from our client and add that data as a new user object 
	//in our users array.

	//This will act as a placeholder for the body of our POST request
	let requestBody = "";
	//Receiving data from a client to a nodejs server requires 2 steps:

	//data step - this part will read the steam of data from our client and process the incoming data into the
	//requestBody variable	

	req.on('data', function(data){
		/*console.log(data);*/
		requestBody += data;

	});
	//end step - this will run once or after the request has been completely sent from our client.
	req.on('end', function(){
		//requestBody now contains the data from our Postman client. However, it is in JSON format.
		//to process our requestBody, we have to parse it into a proper JS object using JSON.parse
		/*console.log(requestBody)*/
		requestBody = JSON.parse(requestBody);

		const newUser = {
			username: requestBody.username,
			email: requestBody.email,
			password: requestBody.password
		}
		users.push(newUser)

		console.log(users);
		res.writeHead(200, {"Content-Type": "application/json"});
		res.end("HELLO user, you are getting the details for our users. " + JSON.stringify(users));
	})
}
else if (req.url === "/users" && req.method === "GET") {
	res.writeHead(200, {"Content-Type": "application/json"});
	res.end("HELLO user, you are getting the details for our users. " + JSON.stringify(users));
}

else if (req.url === "/courses" && req.method === "GET") {
	res.writeHead(200, {"Content-Type": "application/json"});
	res.end("Here are the available courses as of now: " + JSON.stringify(courses));
}
else if (req.url === "/courses" && req.method === "POST") {
	let requestBody = "";
	req.on('data', function(_input){
		
		requestBody += _input;

	});
	req.on('end', function(){
		//requestBody now contains the data from our Postman client. However, it is in JSON format.
		//to process our requestBody, we have to parse it into a proper JS object using JSON.parse
		/*console.log(requestBody)*/
		requestBody = JSON.parse(requestBody);

		const newCourse = {
			name: requestBody.name,
			price: requestBody.price,
			isActive: requestBody.isActive
		}
		courses.push(newCourse)

		res.writeHead(200, {"Content-Type": "application/json"});
		res.end("These are the courses available as of now: " + JSON.stringify(courses));
		console.log(courses);
	})

	/*res.writeHead(200, {"Content-Type": "application/json"});
	res.end("Here are the available courses as of now: " + JSON.stringify(courses));*/
}


}).listen(4000);
console.log("Server is running on localhost: 4000");