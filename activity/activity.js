let http = require("http");
const items =
[
	{
		name: "Iphone X",
		price: 30000,
		isActive: true
	},
	{
		name: "Samsung Galaxy S21",
		price: 51000,
		isActive: true
	},
	{
		name: "Razer Blackshark VX2",
		price: 2800,
		isActive: false
	}
]

http.createServer(function(_req,_res){

if(_req.url === "/items" && _req.method === "GET") {
	_res.writeHead(200, {'Content-Type': 'application/json'});
	_res.end('These are the current items: ' + JSON.stringify(items))
}
else if (_req.url === "/items" && _req.method === "POST"){
	let requestBody = "";
	_req.on('data', function(_data) {
		requestBody += _data;
	})
	_req.on('end', function(){
		requestBody = JSON.parse(requestBody);
		const newItem = {
			name: requestBody.name,
			price: requestBody.price,
			isActive: requestBody.isActive
		}
		items.push(newItem);
		console.log(items);
		_res.writeHead(200, {'Content-Type': 'application/json'});
		_res.end('These are the current items: ' + JSON.stringify(items));

	})
}
else if (_req.url === "/items" && _req.method === "DELETE") {
	items.pop();
	console.log(items);
		_res.writeHead(200, {'Content-Type': 'application/json'});
		_res.end('These are the current items: ' + JSON.stringify(items));

}
}).listen(8000);
console.log("Server is running on localhost: 8000");